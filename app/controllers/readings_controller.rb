class ReadingsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :find_thermostat, only: :create

  ERROR_MESSAGE = 'Could not find the record!'.freeze

  def create
    creator = Services::ReadingCreator.new(params, thermostat)
    if response = creator.process
      render json: { sequence_number: response[:sequence_number], reading_id: response[:reading_id] }, status: :created
    else
      render json: { success: false }, status: :unprocessable_entity
    end
  end

  def show
    uuid = params[:id]
    reading = Reading.find_by(uuid: uuid)
    reading ||= Rails.cache.read(uuid)
    if reading
      render json: { success: true, reading: reading }, status: :ok
    else
      render json: { success: false, message: ERROR_MESSAGE }, status: :unprocessable_entity
    end
  end

  private

  attr_accessor :thermostat

  def find_thermostat
    @thermostat = Rails.cache.read(params[:household_token].to_s)
    @thermostat ||= Thermostat.find_by_household_token(params[:household_token])
    return render_non_authoritative_information unless thermostat
    Rails.cache.write(thermostat.household_token, thermostat, expires_in: 1.day)
  end

  def render_non_authoritative_information
    render json: { success: false, message: ERROR_MESSAGE }, status: :non_authoritative_information
  end
end
