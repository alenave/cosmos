class ThermostatsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :find_thermostat, only: :stats

  ERROR_MESSAGE = 'Could not find the record!'.freeze

  def create
    creator = Services::ThermostatCreator.new(params)
    if creator.process
      render json: { success: true }, status: :created
    else
      render json: { success: false, message: creator.errors.full_messages.first }, status: :unprocessable_entity
    end
  end

  def stats
    render json: [ Services::Stats::Get.new(thermostat.id, Constants::Thermostat::TEMPERATURE).process ,
                   Services::Stats::Get.new(thermostat.id, Constants::Thermostat::HUMIDITY).process ,
                   Services::Stats::Get.new(thermostat.id, Constants::Thermostat::BATTERY_CHARGE).process ]
  end

  private

  attr_reader :thermostat

  def find_thermostat
    @thermostat = Rails.cache.read(params[:household_token].to_s)
    @thermostat ||= Thermostat.find_by_household_token(params[:household_token])
    return render_non_authoritative_information unless thermostat
    Rails.cache.write(thermostat.household_token, thermostat, expires_in: 1.day)
  end

  def render_non_authoritative_information
    render json: { success: false, message: ERROR_MESSAGE }, status: :non_authoritative_information
  end
end
