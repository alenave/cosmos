class CreateReadingWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default

  attr_reader :args, :uuid, :sequence_number, :thermostat

  def perform(args, uuid, sequence_number, thermostat)
    @args = args
    @uuid = uuid
    @sequence_number = sequence_number
    @thermostat = thermostat
    set_total_readings
    reading_creater
  end

  def reading_creater
    Reading.create(sequence_number: sequence_number,
                   temperature: args['temperature'],
                   humidity: args['humidity'],
                   battery_charge: args['battery_charge'],
                   thermostat: thermostat,
                   uuid: uuid)
  end

  def set_total_readings
    count = Rails.cache.read("#{thermostat.id}_total_readings")
    count ||= Reading.where(thermostat: thermostat).count
    count += 1
    Rails.cache.write("#{thermostat.id}_total_readings", count, expires_in: 1.day)
  end
end
