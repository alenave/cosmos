class SetStatsWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical

  def perform(params)
    Services::Stats::Set.new(params[:thermostat_id], Constants::Thermostat::HUMIDITY, params[:humidity]).process
    Services::Stats::Set.new(params[:thermostat_id], Constants::Thermostat::BATTERY_CHARGE, params[:battery_charge])
                        .process
    Services::Stats::Set.new(params[:thermostat_id], Constants::Thermostat::TEMPERATURE, params[:temperature]).process
  end
end
