class CreateReadings < ActiveRecord::Migration[5.0]
  def change
    create_table :readings do |t|
      t.integer :sequence_number
      t.float :temperature
      t.float :humidity
      t.float :battery_charge
      t.string :uuid

      t.references(:thermostat, index: true)

      t.timestamps
    end
  end
end
