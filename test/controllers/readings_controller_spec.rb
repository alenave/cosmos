require 'rails_helper'

RSpec.describe ReadingsController, type: :controller do
  describe '#create' do
    context 'when thermostat is available' do
      it 'creats reading' do
        thermostat = FactoryBot.create(:thermostat)
        params = {
          household_token: thermostat.household_token,
          humidity: 20,
          battery_charge: 65,
          temperature: 23
        }
        post :create, params: params

        expect(response.status).to eq(201)
      end
    end

    context 'when thermostat is not available' do
      it 'return non_authoritative_information' do
        thermostat = FactoryBot.create(:thermostat)
        params = {
          household_token: 'not available',
          humidity: 20,
          battery_charge: 65,
          temperature: 23
        }
        post :create, params: params
        expect(response.status).to eq(203)
      end
    end
  end
end
