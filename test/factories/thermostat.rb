FactoryBot.define do
  factory :thermostat do
    household_token { 'Hello moto' }
    location { 'Bengaluru' }
  end
end
