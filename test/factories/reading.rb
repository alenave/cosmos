FactoryBot.define do
  factory :reading do
    uuid { SecureRandom.uuid }
    sequence_number { 12 }
  end
end
