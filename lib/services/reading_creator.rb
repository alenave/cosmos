module Services
  class ReadingCreator
    def initialize(params, thermostat)
      @params = params
      @thermostat = thermostat
    end

    def process
      create_reading
    end

    private

    attr_reader :params, :sequence_number, :thermostat

    def create_reading
      @sequence_number = Services::GenerateSequenceNumber.new(params[:household_token]).process
      return false unless sequence_number
      Rails.cache.write(uuid, data_to_be_cached, expires_in: 1.minute)
      set_stats
      CreateReadingWorker.perform_async(params.as_json, uuid, sequence_number, thermostat)
      { sequence_number: sequence_number, reading_id: uuid }
    end

    def uuid
      @uuid ||= SecureRandom.uuid
    end

    def data_to_be_cached
      {
        sequence_number: sequence_number,
        temperature: params[:temperature],
        humidity: params[:humidity],
        battery_charge: params[:battery_charge]
      }
    end

    def set_stats
      stats_params = {}
      stats_params[:thermostat_id] = thermostat.id
      stats_params[Constants::Thermostat::HUMIDITY.to_sym] = params[:humidity]
      stats_params[Constants::Thermostat::BATTERY_CHARGE.to_sym] = params[:battery_charge]
      stats_params[Constants::Thermostat::TEMPERATURE.to_sym] = params[:temperature]
      SetStatsWorker.new.perform(stats_params)
    end
  end
end
