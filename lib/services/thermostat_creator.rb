module Services
  class ThermostatCreator
    include ActiveModel::Validations

    validate :unique_thermostat

    ERROR_MESSAGE = 'Huosehold token already available'.freeze

    attr_reader :household_token, :location

    def initialize(params)
      @household_token = params[:household_token]
      @location = params[:location]
    end

    def process
      return unless valid?
      create_thermostat
    end

    private

    def create_thermostat
      thermostat = Thermostat.create(household_token: household_token, location: location)
      Rails.cache.write(household_token, thermostat, expires_in: 1.day)
      Rails.cache.write("#{household_token}_sequence_number", 1, expires_in: 1.day)
      true
    end

    def unique_thermostat
      return unless Thermostat.find_by_household_token(household_token).present?

      errors.add(:base, ERROR_MESSAGE)
    end
  end
end
