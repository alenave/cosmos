module Services
  class GenerateSequenceNumber
    include ActiveModel::Validations

    validate :thermostat_availability

    attr_reader :household_token, :sequence_number

    ERROR_MESSAGE = 'Thermostat is not available'.freeze

    def initialize(household_token)
      @household_token = household_token
    end

    def process
      return unless valid?
      @sequence_number = Rails.cache.read("#{household_token}_sequence_number")
      if sequence_number.present?
        next_sequence_number
        set_sequence_number
        return sequence_number
      end
      @sequence_number = thermostat.readings.last.try(:sequence_number) || 1
      set_sequence_number
      sequence_number
    end

    private

    def next_sequence_number
      @sequence_number += 1
    end

    def set_sequence_number
      Rails.cache.write("#{household_token}_sequence_number", sequence_number, expires_in: 1.day)
    end

    def thermostat_availability
      return if thermostat.present?

      errors.add(:base, ERROR_MESSAGE)
    end

    def thermostat
      @thermostat ||= Rails.cache.read(household_token) || Thermostat.find_by_household_token(household_token)
    end
  end
end
