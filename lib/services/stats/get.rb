module Services
  module Stats
    class Get
      attr_accessor :thermostat_id, :type

      def initialize(thermostat_id, type)
        @thermostat_id = thermostat_id
        @type = type
      end

      def process
        { "#{type}": { average: average, maximum: maximum, minimum: minimum } }
      end

      def average
        previous_avg = Rails.cache.read("#{thermostat_id}_average_#{type}")
        unless previous_avg
          reading = Reading.where(thermostat_id: thermostat_id)
          avg = nil
          avg = Reading.where(thermostat_id: thermostat_id).average(type.to_sym).to_f.round(2) if reading.present?
          Rails.cache.write("#{thermostat_id}_average_#{type}", avg)
          return avg
        end
        previous_avg
      end

      def maximum
        previous_max = Rails.cache.read("#{thermostat_id}_maximum_#{type}")
        unless previous_max
          reading = Reading.where(thermostat_id: thermostat_id)
          max = nil
          max = Reading.where(thermostat_id: thermostat_id).maximum(type.to_sym).to_f if reading.present?
          Rails.cache.write("#{thermostat_id}_maximum_#{type}", max)
          return max
        end
        previous_max
      end

      def minimum
        previous_min = Rails.cache.read("#{thermostat_id}_minimum_#{type}")
        unless  previous_min
          reading = Reading.where(thermostat_id: thermostat_id)
          min = nil
          min = Reading.where(thermostat_id: thermostat_id).minimum(type.to_sym).to_f if reading.present?
          Rails.cache.write("#{thermostat_id}_minimum_#{type}", min)
          return min
        end
        previous_min
      end
    end
  end
end
