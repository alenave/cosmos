module Services
  module Stats
    class Set
      attr_accessor :thermostat_id, :type, :new_reading

      def initialize(thermostat_id, type, new_reading)
        @thermostat_id = thermostat_id
        @type = type
        @new_reading = new_reading.to_i
      end

      def process
        set_new_average
        set_new_maximum
        set_new_minimum
        true
      end

      private

      attr_reader :average, :min, :max

      def set_new_average
        average = Services::Stats::Get.new(thermostat_id, type).average || new_reading
        average = ((average * total_readings + new_reading) / (total_readings.to_f + 1)).round(2)
        Rails.cache.write("#{thermostat_id}_average_#{type}", average)
      end

      def set_new_maximum
        previous_max = Services::Stats::Get.new(thermostat_id, type).maximum || new_reading
        Rails.cache.write("#{thermostat_id}_maximum_#{type}", previous_max > new_reading ? previous_max : new_reading)
      end

      def set_new_minimum
        previous_min = Services::Stats::Get.new(thermostat_id, type).minimum || new_reading
        Rails.cache.write("#{thermostat_id}_minimum_#{type}", previous_min < new_reading ? previous_min : new_reading)
      end

      def total_readings
        count = Rails.cache.read("#{thermostat_id}_total_readings")
        return count = Reading.where(thermostat_id: thermostat_id).count unless count
        count
      end
    end
  end
end
