module Constants
  module Thermostat
    HUMIDITY = 'humidity'.freeze
    TEMPERATURE = 'temperature'.freeze
    BATTERY_CHARGE = 'battery_charge'.freeze
  end
end
