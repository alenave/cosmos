# COSMOS


### Prerequisites

* Install [MySQL](https://dev.mysql.com/downloads/mysql/)
* Install [Redis](http://redis.io/download)

* Ruby version 2.2.7

* Rails version 5.0.7

## Setting up the database

* Migrate the database using 
    ```
    bundle exec rake db:drop db:create db:migrate
    ```

* Seed the database using 
    ```
    bundle exec rake db:seed
    ```

* Set up the test database

  ```
  RAILS_ENV=test rake db:drop db:create db:migrate db:seed
  ```
  
* Add the server ip and port number for redis in the test environment in your config file

* Start the redis server using 
    ```
    brew services start redis
    ```